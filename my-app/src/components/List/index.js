import './List.css';


export default function List(){
    const planets=["Mercury", "Venus", "Earth", "Mars", "Jupiter", "Sarturn", "Uranus", "Neptune"].map((item,i)=>({id:i, text:item}));
    return (
        <ul className="planets-list">
          {planets.map(item=>
          (
            <li key={item.id}>{item.text}</li>
          ))}
          
        </ul>
    )
}