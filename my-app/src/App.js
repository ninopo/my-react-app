import './App.css';
import React from 'react';
import List from './components/List';

function App() {
  const changeTheme = ()=>{
    const body = document.querySelector('body');
    const list = document.querySelectorAll('li');
    body.classList.toggle('dark');
    if(body.classList.contains('dark')){
      list.forEach(item=>item.style.color = "#66bb6a");
    }else{
      list.forEach(item=>item.style.color = "black");
    }
    
 }

  return (
    <div className='App'>
 <label className="switch" htmlFor="checkbox">
    <input type="checkbox" id="checkbox" onClick={changeTheme}  />
    <div className="slider round"></div>
</label>
   { React.createElement('h1', {style: { color: '#999', fontSize: '19px'}}, 'Solar system planets')}

   <List/>

   </div>
    
  );
  
}

export default App;
